# chat/consumers.py

import aiojobs
import asyncio
import base64
import ffmpeg
import logging

from channels.generic.websocket import AsyncJsonWebsocketConsumer
from grpclib.client import Channel

from protosa.asr.asr_decoder_server_grpc import AsrOnlineDecoderStub
from protosa.asr.asr_decoder_server_pb2 import TranscriptStatus
from protosa.speech.audio_pb2 import Audio

LOG = logging.getLogger(__name__)


default_streaming_config = {
    "interim_results": True
}


async def format_audio(data=None, input_data="pipe:", in_props={}, out_props={}):
    if input_data == "pipe:":
        pipe_stdin = True
    else:
        pipe_stdin = False

    try:
        proc = (
            ffmpeg.input(input_data, **in_props)
            .output("pipe:", **out_props)
            .global_args('-loglevel', 'quiet')
            .run_async(pipe_stdin=pipe_stdin, pipe_stdout=True)
        )
        out, _ = proc.communicate(input=data)
    except Exception as e:
        LOG.error(f"ffmpeg call error.")
        raise e

    return out


def process_streaming_config(message):
    if "streaming_config" not in message:
        error_message = {
            "error": {
                "message": "'streaming_config' must exist in the first message!"
            }
        }
        return False, error_message

    incoming_config = message["streaming_config"]
    if "interim_results" not in incoming_config:
        incoming_config["interim_results"] = default_streaming_config["interim_results"]

    if "single_utterance" not in incoming_config:
        incoming_config["single_utterance"] = default_streaming_config["single_utterance"]

    return True, incoming_config


async def process_audio_content(message):
    if "audio_content" not in message:
        error_message = {
            "error": {
                "message": "'audio_content' must exist in the subsequent messages!"
            }
        }
        return False, error_message

    if message["audio_content"] != "":
        audio_data = message["audio_content"].encode("utf-8")
        audio_bytes = base64.b64decode(audio_data)
        out_props = {
            "f": "s16le",
            "acodec": "pcm_s16le",
            "ac": 1,
            "ar": 16000,
            "flags": "+bitexact",
        }
        data = await format_audio(audio_bytes, out_props=out_props)
    else:
        data = b""

    return True, data


class ChatConsumer(AsyncJsonWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Initialize gRPC channel & stub
        self.grpc_channel = Channel("0.0.0.0", 52078)
        self.asr_stub = AsrOnlineDecoderStub(self.grpc_channel)
        self.grcp_stream = None

        # config for response sending (to client)
        self.streaming_config = None

        # vars to manage state
        self.first_message_received = False
        self.receiving_response = False
        self.stream_ended = False

        # scheduler for response retrieval (from engine)
        self.scheduler = None

    async def connect(self):
        self.scheduler = await aiojobs.create_scheduler()
        await self.scheduler.spawn(self.send_transcript())

        async with self.asr_stub.StreamWords.open() as stream:
            self.grcp_stream = stream

        await self.accept()
        LOG.info("Websocket connection established")

    async def disconnect(self, close_code):
        await self.scheduler.close()
        self.grpc_channel.close()
        LOG.info("Websocket connection closed")

    async def send_transcript(self):
        LOG.info("gRPC response listener started")
        while True:
            if (self.receiving_response):
                async for response in self.grcp_stream:
                    LOG.info(response.words)

                    is_final = response.status == TranscriptStatus.FINAL
                    transcription = {
                        "words": response.words,
                        "is_final": is_final
                    }

                    if is_final or self.streaming_config["interim_results"]:
                        await self.send_json(transcription)

                await self.close(1000)
                break
            else:
                await asyncio.sleep(1.0)

            await asyncio.sleep(1.0)

    async def receive_json(self, content):
        if not self.first_message_received:
            is_valid, result = process_streaming_config(content)

            if not is_valid:
                await self.send_json(result, close=True)

            self.streaming_config = result
            self.first_message_received = True
        else:
            is_valid, result = await process_audio_content(content)
            if not is_valid:
                await self.send_json(result, close=True)

            if len(result) > 0:
                voice = Audio(data=result, samplerate=16000)
                await self.grcp_stream.send_message(voice)
                if not self.receiving_response:
                    self.receiving_response = True
            else:
                if not self.stream_ended:
                    LOG.info("Closing stream")
                    await self.grcp_stream.end()
                    self.stream_ended = True

            await asyncio.sleep(1.0)
